#include <string>
#include "json11.hpp"


int main() {
  json11::Json my_json = json11::Json::object {
    { "key1", "value1" },
    { "key2", false },
    { "key3", json11::Json::array { 1, 2, 3 } },
  };
  std::string json_str = my_json.dump();
  return 0;
}
