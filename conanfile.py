from conans import ConanFile, CMake, tools
import os


class Json11Conan(ConanFile):
    name = "json11"
    version = "0e409dc"
    changelist = "0e409dca350048406767e90dd16841d09df13807"
    license = "MIT License"
    url = "https://bitbucket.org/genvidtech/conan-json11"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    description = "A small json c++11 library."
    exports = "msvc.patch"

    def source(self):
        self.run("git clone https://github.com/dropbox/json11")
        self.run("cd json11 && git checkout %s" % self.version)
        if self.settings.compiler == "Visual Studio":
            tools.patch(base_path="json11", patch_file="msvc.patch", strip=1)
        tools.replace_in_file("json11/CMakeLists.txt", "enable_testing()", '''enable_testing()
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')
        

    def build(self):
        cmake = CMake(self.settings)
        shared = "-DBUILD_SHARED_LIBS=ON" if self.options.shared else ""
        self.run('cmake json11 %s %s' % (cmake.command_line, shared))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.hpp", dst="include", src="json11")
        self.copy("*json11.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["json11"]
